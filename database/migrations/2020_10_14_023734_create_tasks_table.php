<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('uid');
            $table->unsignedInteger('creator')->nullable(false);
            $table->unsignedInteger('performer')->nullable();
            $table->unsignedSmallInteger('level')->nullable(false);
            $table->text('text')->nullable(false);
            $table->timestamp('execute_until')->nullable(false);
            $table->timestamps();
            $table->timestamp('finished_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tasks');
    }
}
