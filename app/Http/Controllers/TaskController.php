<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskResource;
use App\Http\Resources\TasksResource;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @author Исаков Владислав
     */
    public function create(Request $request) {
        $userName = $request->header('Task-User');
        if (null === $userName) {
            return response(['error' => 'Пользователь не определен'], 422);
        }

        if (false === array_key_exists($userName, User::TEST_USERS)) {
            return response(['error' => 'Пользователь не определен'], 422);
        }

        $data = json_decode($request->getContent(), true);

        $task = new Task();
        $task->creator = User::TEST_USERS[$userName];
        $task->performer = $data['performer'];
        $task->level = $data['level'];
        $task->text = $data['text'];
        $task->execute_until = $data['execute_until'];

        if ($task->validate($data) && $task->save()) {
            return response(['uid' => $task->uid]);
        }

        return response(['error' => 'Ошибка создания задачи: ' . $task->errors], 422);
    }

    /**
     * @param Request $request
     * @return TasksResource
     * @author Исаков Владислав
     */
    public function list(Request $request) {
        $userName = $request->header('Task-User');

        if (null !== $userName && array_key_exists($userName, User::TEST_USERS)) {
            $tasks = Task::where('performer', User::TEST_USERS[$userName])->get();
        } else {
            $tasks = Task::all();
        }

        return new TasksResource($tasks);
    }

    /**
     * @param int $uid
     * @return TaskResource|array
     * @author Исаков Владислав
     */
    public function show($uid) {
        $task = Task::find($uid);

        if (null === $task) {
            return ['error' => 'Задача не найдена'];
        }

        return new TaskResource($task);
    }

    /**
     * @param Request $request
     * @param int $uid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @author Исаков Владислав
     */
    public function finish(Request $request, $uid) {
        $userName = $request->header('Task-User');

        if (null === $userName) {
            return response(['error' => 'Пользователь не определен'], 422);
        }

        if (false === array_key_exists($userName, User::TEST_USERS)) {
            return response(['error' => 'Пользователь не определен'], 422);
        }

        /** @var Task $task */
        $task = Task::find($uid);

        if (null === $task) {
            return ['error' => 'Задача не найдена'];
        }

        if (User::TEST_USERS[$userName] === $task->performer || User::TEST_USERS[$userName] === $task->creator) {
            $task->finished_at = DB::raw('now()');
            $task->save();

            return response([], 200);
        }

        return response(['error' => 'Ошибка завершения задачи'], 422);
    }

    /**
     * @param Request $request
     * @param int $uid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @author Исаков Владислав
     */
    public function edit(Request $request, $uid) {
        $userName = $request->header('Task-User');

        if (null === $userName) {
            return response(['error' => 'Пользователь не определен'], 422);
        }

        if (false === array_key_exists($userName, User::TEST_USERS)) {
            return response(['error' => 'Пользователь не определен'], 422);
        }

        /** @var Task $task */
        $task = Task::find($uid);

        if (null === $task) {
            return response(['error' => 'Задача не найдена'], 422);
        }

        if ($task->performer !== User::TEST_USERS[$userName] && $task->creator !== User::TEST_USERS[$userName]) {
            return response(['error' => 'Вы не можете редактировать эту задачу'], 422);
        }

        $data = json_decode($request->getContent(), true);

        $task->level = $data['level'];
        $task->execute_until = $data['execute_until'];

        if (User::TEST_USERS[$userName] === $task->creator) {
            $task->performer = $data['performer'];
            $task->text = $data['text'];
        }

        if ($task->validate($data) && $task->save()) {
            return new TaskResource($task);
        }

        return response(['error' => 'Ошибка изменения задачи: ' . $task->errors], 422);
    }
}
