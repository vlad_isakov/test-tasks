<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TasksResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) {
        foreach ($this->collection as $item) {
            $item->text = substr($item->text, 0, 100);
        }

        return [
            TaskResource::collection($this->collection),
        ];
    }
}
