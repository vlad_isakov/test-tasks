<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) {
         return [
            'uid'           => $this->uid,
            'creator'       => $this->creator,
            'performer'     => $this->performer,
            'level'         => $this->level,
            'text'          => $this->text,
            'execute_until' => $this->execute_until,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'finished_at'   => $this->finished_at,
        ];
    }
}
