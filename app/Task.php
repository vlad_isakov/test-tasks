<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * Class Task
 *
 * @package App
 * @property int $uid
 * @property int $creator
 * @property int $performer
 * @property int $level
 * @property string $text
 * @property string $execute_until
 * @property string $created_at
 * @property string $updated_at
 * @property string $finished_at
 *
 * @property array $errors
 *
 * @author Исаков Владислав
 */
class Task extends Model
{
    protected $primaryKey = 'uid';

    protected $rules = [
        'text'          => 'required|max:1000',
        'creator'       => 'required',
        'performer'     => 'required',
        'level'         => 'required',
        'execute_until' => 'required',
    ];

    /**
     * @param array $inputs
     * @return bool
     * @author Исаков Владислав
     */
    public function validate($inputs) {
        $v = Validator::make($inputs, $this->rules);
        if ($v->passes()) {
            return true;
        }

        $this->errors = $v->messages();

        return false;
    }
}
