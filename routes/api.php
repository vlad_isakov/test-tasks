<?php
use Illuminate\Http\Request;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/task/list', [TaskController::class, 'list']);
Route::get('/task/show/{uid}', [TaskController::class, 'show']);
Route::post('/task/create', [TaskController::class, 'create']);
Route::post('/task/edit/{uid}', [TaskController::class, 'edit']);
Route::post('/task/finish/{uid}', [TaskController::class, 'finish']);
